import React , {Component} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import './haveacc.css';

const server = 'www.thecocircle.com';
const serverphp = 'www.thecocircle.com';


export default class haveacc extends Component {

constructor(props) {
  super(props);
  this.state = {
    
  };

}

render(){

  return(
    <div className="outer">
    <div className="middle">
        <div className="inner" align="center">

        <h1>You already have an account in our database please login.</h1>
        <br/>
        <RaisedButton onClick={()=> window.location.replace('https://'+server)} label="LOGIN" primary={true}/>

        </div>
    </div>
    </div>
  );
}
}
