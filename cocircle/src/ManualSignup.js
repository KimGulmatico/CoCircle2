import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import axios from 'axios';
import Fin from 'mui-icons/evilicons/check';
import * as moment from 'moment';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import './ManualSignup.css';

const server = 'www.thecocircle.com';
const serverphp = 'www.thecocircle.com';


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};

const customContentStyle = {
  width: '95%',
  maxWidth: '700px',
}

export default class ManualSignup extends Component {

constructor(props) {
  super(props);
  this.state = {
    lname: null,
    fname: null,
    email: null,
    mobile: null,
    bday: null,
    pass: null,
    open: false,
  };
  this.handleInsert = this.handleInsert.bind(this);
  
}

componentDidMount() {
  window.scrollTo(0, 0);
}


handleChangeLname = (event) => {
    this.setState({
      lname: event.target.value,
    });
};


handleChangeFname = (event) => {
    this.setState({
      fname: event.target.value,
    });
};


handleChangeEmail = (event) => {
    this.setState({
      email: event.target.value,
    });
};

handleChangeMobile = (event) => {
    this.setState({
      mobile: event.target.value,
    });
};

handleChangeDate = (event, date) => {
    this.setState({
      bday: date,
    });
};
handleChangePass= (event) => {
    this.setState({
      pass: event.target.value,
    });
};

_handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.setState({open: true})
    }
}

handleOpen = () => {
    this.setState({open: true});
};

handleClose = () => {
    this.setState({open: false});
};

handleInsert(){
    axios.get('https://'+serverphp+'/php/insertManualSignup.php?user='+JSON.stringify(this.state))
      .then(res => {
        if(String(res.data).trim() == '1'){
            localStorage.setItem('user',this.state.email);
            localStorage.setItem('pass',this.state.pass);
            this.setState({open: false});
            
            window.location.replace("https://"+server+"/User");
        }
        else if(String(res.data).trim() == '2'){
            window.location.replace('https://'+server+'/Haveaccount');
        }
        else{
            alert(res.data);
        }
    });
}

render(){ 
const actions = [
    <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
    />,
    <FlatButton
        label="Accept"
        primary={true}
        onClick={this.handleInsert}
    />,
];

const Manual = <div>
                  <div className="blockdiv">
                  <h2 className="bluetxts roboto">REGISTER</h2>
                  <TextField  value={this.state.lname} onChange={this.handleChangeLname} floatingLabelText="Lastname" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <TextField  type="text" value={this.state.fname} onChange={this.handleChangeFname} floatingLabelText="Firstname" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <TextField  type="text" value={this.state.email} onChange={this.handleChangeEmail} floatingLabelText="Email" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <TextField  type="number" value={this.state.mobile} onChange={this.handleChangeMobile} floatingLabelText="Mobile Number" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <br/>
                  <TextField type="date" floatingLabelFixed="true" floatingLabelText="Birthday" fullWidth="true" value={this.state.bday} onChange={this.handleChangeDate} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <TextField  type="password" value={this.state.pass} onKeyPress={this._handleKeyPress} onChange={this.handleChangePass} floatingLabelText="New Password" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <br/>
                  <br/>
                  <RaisedButton label="SIGN UP" primary={true} className="btncenter" onClick={this.handleOpen}/>   
                  <div align="center">
                    <br/>
                    <a className="havacc" onClick={() => window.location.replace("https://"+server)}>Go back</a>
                    <br/>
                  </div>     
                  </div>
                </div>

  return (
      <div>
        <div className="body">
            <div className="back">
              
            </div>
        </div>
        <div className = "logindivmanual">
          <div className="contentmanual">
            <Avatar src={require('./images/Logo.png')} size={100} className="logo" backgroundColor="white"/>
            <div className="childcontentmanual">
              <br/>
              <br/>
              <br/>
              {Manual}
            </div>
        </div>
        </div>

        <Dialog
          title="Terms and Conditions, Privacy Policy"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          contentStyle={customContentStyle}
        >   
            <iframe style={{'margin-top': '-43px', width: '100%', height: '25460px','max-height': '25460px', border: 'none'}} scrolling="no" src="https://docs.google.com/document/d/e/2PACX-1vQ_huL6WCVmfz4L78yioG5PIax_PkBQAJZ_hUUK3ZucJX25voM--1x7ItSZM-xNp6TGKo7nNP-HH6ji/pub"></iframe>
        </Dialog>
      </div>);
}
}

