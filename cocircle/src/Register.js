import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import axios from 'axios';
import Fin from 'mui-icons/evilicons/check';
import * as moment from 'moment';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';

import './Register.css';

const server = 'www.thecocircle.com';
const serverphp = 'www.thecocircle.com';


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};

const customContentStyle = {
  width: '95%',
  maxWidth: '700px',
}

export default class Register extends Component {

constructor(props) {
  super(props);
  this.state = {
    finish: false,
    mobile: null,
    bday: null,
    pass: null,
    id: null,
    user: null,
    open: false,
  };

  //this.handleChange = this.handleChange.bind(this);
  this.handleInsert = this.handleInsert.bind(this);
}

componentDidMount() {
  window.scrollTo(0, 0);
  //var retrievedObject = localStorage.getItem('testObject');
  //console.log(JSON.parse(retrievedObject));
  //this.setState({ user: JSON.parse(retrievedObject)});
}

handleChangeMobile = (event) => {
    this.setState({
      mobile: event.target.value,
    });
};

handleChangeDate = (event, date) => {
    this.setState({
      bday: date,
    });
};
handleChangePass= (event) => {
    this.setState({
      pass: event.target.value,
    });
};

handleCheckSession(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
}

handleInsert(e) {
  //e.preventDefault();
  var user = JSON.parse(localStorage.getItem('testObject'));
  //console.log(user);
  const data = new FormData();
  data.append('pictureUrl', typeof user.pictureUrl !== "undefined"? user.pictureUrl:'https://thecocircle.com/images/avatar2.jpg');
  data.append('mobile', this.state.mobile);
  data.append('bday',  moment(this.state.bday).format('YYYY-MM-DD HH:mm:ss'));
  data.append('pass', this.state.pass);
  data.append('id', typeof user.id !== "undefined"?user.id:'');
  data.append('fname', typeof user.firstName !== "undefined"?user.firstName:'');
  data.append('lname', typeof user.lastName !== "undefined"?user.lastName:'');
  data.append('headline', typeof user.headline !== "undefined"?user.headline:'');
  data.append('email', typeof user.emailAddress !== "undefined"?user.emailAddress:'');
  data.append('countryname', typeof user.location.name !== "undefined"?user.location.name:'');
  data.append('publicProfileUrl', typeof user.publicProfileUrl !== "undefined"?user.publicProfileUrl:'');
  data.append('specialties', typeof user.specialties !== "undefined"?user.specialties:'');
  data.append('summary', typeof user.summary !== "undefined"?user.summary:'');
  //alert(user.positions._total);
  data.append('companyID', user.positions._total !== 0?user.positions.values[0].company.id:'');
  data.append('companyName', user.positions._total !== 0?user.positions.values[0].company.name:'');
  data.append('industry', user.positions._total !== 0?user.positions.values[0].company.industry:'');
  data.append('companySize', user.positions._total !== 0?user.positions.values[0].company.size:'');
  data.append('companyisCurrent', user.positions._total !== 0?user.positions.values[0].isCurrent:'');
  data.append('companyStartDate', user.positions._total !== 0?user.positions.values[0].startDate.month:'');
  data.append('companyYear', user.positions._total !== 0?user.positions.values[0].startDate.year:'');
  data.append('companySummary', user.positions._total !== 0?user.positions.values[0].summary:'');
  data.append('companyLocation', user.positions._total !== 0?user.positions.values[0].location.name:'');
  data.append('companyTitle', user.positions._total !== 0?user.positions.values[0].title:'');
  data.append('companySector', user.positions._total !== 0?user.positions.values[0].company.sector:'');

  

  axios.post('https://'+serverphp+'/php/insertCustomer.php',data)
  .then(res => {
      if(String(res.data).trim() == '1'){
        localStorage.setItem('user',this.state.mobile);
        localStorage.setItem('pass',this.state.pass);
        this.setState({open: false});
        window.location.replace("https://"+server+"/User");

      }else if(String(res.data).trim() == 'alreadyuser'){
        window.location.replace('https://'+server+'/Haveaccount');
      }
      else{
        alert(res.data);
      }
  });

}

_handleKeyPress = (e) => {
  if (e.key === 'Enter') {
    this.setState({open: true})
  }
}

handleOpen = () => {
  this.setState({open: true});
};

handleClose = () => {
  this.setState({open: false});
};


render(){

const actions = [
    <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
    />,
    <FlatButton
        label="Accept"
        primary={true}
        onClick={this.handleInsert}
    />,
];

const RegFin = <div align="center">
                <Fin color="#8BC34A" className="finish"/>
                <h3 className="regsuc">Registration Successfull!</h3>
                <br/>
                <br/>
                <br/>
                <a className="havacc" onClick={() => window.location.replace("https://"+server)}>Login</a>
              </div>

const RegPass = <div>
                  <div className="blockdiv">
                  <h2 className="bluetxts roboto">REGISTER</h2>
                  <TextField type="number" value={this.state.mobile} onChange={this.handleChangeMobile} floatingLabelText="Mobile Number" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <br/>
                  <DatePicker floatingLabelText="Birthday" fullWidth="true" value={this.state.bday} onChange={this.handleChangeDate} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <TextField  type="password" value={this.state.pass} onKeyPress={this._handleKeyPress} onChange={this.handleChangePass} floatingLabelText="New Password" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  <br/>
                  <br/>
                  <RaisedButton label="SIGN UP" primary={true} className="btncenter" onClick={this.handleOpen}/>   
                  <div align="center">
                    <br/>
                    <a className="havacc" onClick={() => window.location.replace("https://"+server)}>Go back</a>
                    <br/>
                  </div>     
                  </div>
                </div>




  return (
      <div>
        <div className="body">
            <div className="back">
              
            </div>
        </div>
        <div className = "logindiv">
          <div className="content">
            <Avatar src={require('./images/Logo.png')} size={100} className="logo" backgroundColor="white"/>
            <div className="childcontent">
              <br/>
              <br/>
              <br/>
              {this.state.finish ? RegFin : RegPass}
            </div>
        </div>
        </div>


        <Dialog
          title="Terms and Conditions, Privacy Policy"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          contentStyle={customContentStyle}
        >   
            <iframe style={{'margin-top': '-43px', width: '100%', height: '25460px','max-height': '25460px', border: 'none'}} scrolling="no" src="https://docs.google.com/document/d/e/2PACX-1vQ_huL6WCVmfz4L78yioG5PIax_PkBQAJZ_hUUK3ZucJX25voM--1x7ItSZM-xNp6TGKo7nNP-HH6ji/pub"></iframe>
        </Dialog>
      </div>);
}
}

