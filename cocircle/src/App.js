import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import Register from './Register';
import SuperAdminAcc from './SuperAdminAcc';
import AdminAcc from './AdminAcc';
import AccessCode from './AccessCode';
import RegAdmin from './RegAdmin';
import Haveacc from './haveacc';
import User from './Customer';
import Manual from './ManualSignup';
import './App.css';


const muiTheme = getMuiTheme({
  palette: {
    primary1Color: blue500,
    primary2Color: blue700,
    primary3Color: blue100,
  },
  menuItem: {
    selectedTextColor: blue500,
  },
  tabs: {
    backgroundColor: 'white',
  },
  inkBar: {
    backgroundColor: blue500
  }
  //userAgent: req.headers['user-agent'],
});

const App = () => (
  <MuiThemeProvider muiTheme={muiTheme}>
    <Switch>
    <Route exact path = "/" component={Login}/>
    <Route path = "/Register" component={Register}/>
    <Route path = "/SuperAdmin" component={SuperAdminAcc}/>
    <Route path = "/Admin" component={AdminAcc}/>
    <Route path = "/RegAdmin" component={RegAdmin}/>
    <Route path = "/Haveaccount" component={Haveacc}/>
    <Route path = "/AccessCode" component={AccessCode}/>
    <Route path = "/User" component={User}/>
    <Route path = "/Manual" component={Manual}/>
    </Switch>
  </MuiThemeProvider>
);

export default App;


 
