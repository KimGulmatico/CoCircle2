import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import './AccessCode.css';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import axios from 'axios';
import Fin from 'mui-icons/evilicons/check';

const server = 'www.thecocircle.com';
const serverphp = 'www.thecocircle.com';

// const server = 'localhost:3000';
// const serverphp = 'localhost:80';


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};

export default class AccessCode extends Component {

constructor(props) {
  super(props);
  this.state = {
    finish: false,
    mobile: null,
    bday: null,
    pass: null,
    code: '',
  };
  this.handleCheckAccessCode = this.handleCheckAccessCode.bind(this);
}

componentDidMount() {
  window.scrollTo(0, 0);
}

handleAccessCode = (event) => {
    this.setState({
      code: event.target.value,
    });
};

handleChangeDate = (event, date) => {
    this.setState({
      bday: date,
    });
};
handleChangePass= (event) => {
    this.setState({
      pass: event.target.value,
    });
};


handleCheckAccessCode(event) {
  axios.get('https://'+serverphp+'/php/checkAccessCode.php?x='+this.state.code)
    .then(res => {
      if(res.data == '1'){
        localStorage.setItem('code',this.state.code);
        window.location.href = "https://"+server+"/RegAdmin";
      }
      else{
         localStorage.setItem('user','');
         localStorage.setItem('pass','');
      }
  });
}


render(){

  return (
      <div>
        <div className="body">
        <div className="back">
          
        </div>
        </div>
        <div className="contmainp">
          <div className="contchildp" >
            <Avatar src={require('./images/Logo.png')} size={100} className="logos" backgroundColor="white"/>
            <div className="childcontentp">
              <br/>
              <br/>
              <br/>
              <br/>
              <div>
                  <div className="blockdivsp">
                    <h2 className="bluetxt" style={{color: blue500}}>ACCESS CODE</h2>
                    <TextField  value={this.state.code} onChange={this.handleAccessCode} floatingLabelText="Enter Access code" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  </div>
                  <div >
                    <RaisedButton label="SUBMIT" primary={true} className="btncenter" onClick={this.handleCheckAccessCode}/>
                  </div> 
                  <br/>
                  <div align="center">
                    <a className="havacc" onClick={() => window.location.replace("https://"+server)}>Login</a>
                    <br/>
                    <br/>
                    <br/>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    );


}

}

