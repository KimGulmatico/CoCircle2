import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import './Customer.css';
import 'bootstrap/dist/css/bootstrap.css';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import DotStat from 'mui-icons/fontawesome/circle';
import Arrow from 'mui-icons/fontawesome/chevron-down';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Moreexpand from 'material-ui/svg-icons/navigation/expand-more';
import AddCart from 'material-ui/svg-icons/action/add-shopping-cart';
import Redeem from 'material-ui/svg-icons/action/redeem';
import Star from 'material-ui/svg-icons/toggle/star';
import axios from 'axios';
import renderHTML from 'react-render-html';
import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import {List, ListItem} from 'material-ui/List';
import Tabs, {Tab} from 'material-ui/Tabs';
import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';
import Subheader from 'material-ui/Subheader';
import Dotdotdot from 'react-clamp';
import { Collapse} from 'reactstrap';
import HeadRoom from 'react-headroom';
import SelectField from 'material-ui/SelectField';
import SwipeableViews from 'react-swipeable-views';
import CircularProgress from 'material-ui/CircularProgress';
import SearchIcon from 'material-ui/svg-icons/action/search';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentEdit from 'material-ui/svg-icons/image/edit';
import ContentSave from 'material-ui/svg-icons/content/save';
import Snackbar from 'material-ui/Snackbar';

const server = 'www.thecocircle.com';
const serverphp = 'www.thecocircle.com';


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
  hintText:{
    color: '#9e9e9e',
  },
  smallIcon: {
    width: 30,
    height: 30,
    color: '#757575',
  },
  mediumIcon: {
    width: 48,
    height: 48,
    color: '#757575',
  },
  small: {
    width: 50,
    height: 50,
    padding: 0,
  },
  medium: {
    width: 96,
    height: 96,
    padding: 24,
  },
};

const dialogStyle = {
  width: '95%',
  maxWidth: '400px',
};

const style = {
  position: 'fixed !important',
  bottom: '4%',
  right: '3%',
  'z-index': '99 !important',
}


export default class Customer extends Component {

constructor(props) {
    super(props);

    this.state = {
        paperdiv: '90%',
        user: null,
        pass: null,
        iname: '',
        iprice: '',
        itotal: '',
        more: null,
        openpopup: false,
        t1: blue500,
        t2: '#9e9e9e',
        t3: '#9e9e9e',
        collapse: [],
        mrg: '15px',
        opendlred: false,
        valueQuan: 1,
        showUI: null,
        slideIndex: 0,
        svheight: 'auto',
        session: false,
        points: 0,
        origprice: '',
        descstate: [],
        info: [],
        items: [],
        history: [],
        loadersize: 40,
        loaderthick: 4,
        srchadrs: null,
        admincpny: [],
        updateprofbtn: false,
        openalertedit: false,
        alertmsg: '',
        showsave: false,
        editdisable: true,
        fname: '',
        lname: '',
        bday: '',
        cpnyName: '',
        cpnySector: '',
        cpnySize: '',
        cpnyStartDate: '',
        cpnyTitle: '',
        cpnySummary: '',
        cpnyLoc: '',
        cpnyContact: '',
        cpnyWebsite: '',
        cpnyDesc: '',
        cpnyLogoURL: '',
        disableOrder: false,
        openResOrder: false,
        orderResult: '',
      };

    this.toggle = this.toggle.bind(this);
    this.handlePopupMore = this.handlePopupMore.bind(this);
    this.handleClosePopupMore = this.handleClosePopupMore.bind(this);
    this.handleGetUserInfo = this.handleGetUserInfo.bind(this);
    this.handleGetUserPoints = this.handleGetUserPoints.bind(this);
    this.handleItemListChange = this.handleItemListChange.bind(this);
    this.handleOpenDialog = this.handleOpenDialog.bind(this);
    this.handleOrderItem = this.handleOrderItem.bind(this);
    this.handleChangeQuan = this.handleChangeQuan.bind(this);
    this.handleLoadTransactionHistory = this.handleLoadTransactionHistory.bind(this);
    this.handleKeyDownSrchAdrs = this.handleKeyDownSrchAdrs.bind(this);
    this.srchChangeAdrs = this.srchChangeAdrs.bind(this);
    this.hideDropdown = this.hideDropdown.bind(this);
    this.allowEditProfile = this.allowEditProfile.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
}
handleChangeTab = (value) => {
  var clientHeight;
  if(value == 0){
    this.setState({t1: blue500, t2: '#9e9e9e', t3: '#9e9e9e', updateprofbtn: false});
    clientHeight = document.getElementById('div1').clientHeight;
  }
  if(value == 1){
    this.setState({t1: '#9e9e9e', t2: blue500, t3: '#9e9e9e', updateprofbtn: false});
    clientHeight = document.getElementById('div2').clientHeight;
  }
  if(value == 2){
    this.setState({t1: '#9e9e9e', t2: '#9e9e9e', t3: blue500, updateprofbtn: true});
    clientHeight = document.getElementById('div3').clientHeight;
  }

  this.setState({
    slideIndex: value,
    svheight: clientHeight+15,
  });
};


handlePopupMore(event){
  event.preventDefault();
  this.setState({
      openpopup: true,
      anchorEl: event.currentTarget,
  });
};

handleClosePopupMore(){
  this.setState({
  openpopup: false,
  });
};

handleOpenDialog(name, price){

  var pt = parseInt(this.state.points);
  var total = pt - parseInt(price);
  this.setState({opendlred: true, iname: name, iprice: price, itotal: total, origprice: price});
  
};

handleCloseDialog = () => {
  this.setState({opendlred: false});
};

handleChangeQuan(event, index, value){
  var pt = parseInt(this.state.points);
  var sub = (parseInt(this.state.origprice) * parseInt(value));
  var total = pt - sub;
  this.setState({itotal: total, iprice: sub});
  this.setState({valueQuan: value});
}

updateDimensions() {
  if(window.innerWidth < 770) {
    this.setState({more: false});
    this.setState({mrg: '15px 15px 0 15px'});
    this.setState({loadersize: 50, loaderthick: 4,});
    this.setState({paperdiv: document.body.clientWidth});
  }
  else{
    this.setState({more: true});
    this.setState({mrg: '15px 7.5px 0 7.5px'});
    this.setState({loadersize: 80, loaderthick: 5,});
    this.setState({paperdiv: '110%'});
  } 
}

componentDidMount() {
  this.interval = setInterval(() => this.handleCheckSession(), 1000);
  this.interval = setInterval(() => this.handleGetUserPoints(), 1000);
  this.interval = setInterval(() => this.handleLoadTransactionHistory(), 1000);
  this.updateDimensions();
  window.addEventListener("resize", this.updateDimensions.bind(this));
  this.handleGetUserInfo();
}

componentWillUnmount() {
  window.removeEventListener("resize", this.updateDimensions.bind(this));
}

handleSignout(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get('https://'+serverphp+'/php/signout.php?user='+user+'&pass='+pass)
    .then(res => {
        localStorage.setItem('user','');
        localStorage.setItem('pass','');
  });
} 

handleCheckSession(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  if(this.state.user == null && this.state.pass == null){
    this.setState({user: user});
    this.setState({pass: pass});
  }
  if(user != "" && pass != ""){
  axios.get('https://'+serverphp+'/php/checkSessionLogin.php?user='+user+'&pass='+pass)
    .then(res => {
        if(res.data.trim() == 'admin'){
          window.location.replace('https://'+server+'/Admin');
        }
        else if(res.data.trim() == 'superadmin'){
          window.location.replace('https://'+server+'/SuperAdmin');
        }
        else if(res.data.trim() == 'C'){
          if(this.state.session != true){
          this.setState({session: true});
          }
        }
        else{
           window.location.replace('https://'+server+'/');
        }
  });
  }
  else{
    window.location.replace('https://'+server+'/');
  }
}

handleGetUserInfo(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get('https://'+serverphp+'/php/getCustomerInfo.php?user='+user+'&pass='+pass)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
        this.setState({info: info});
        this.setState({
          fname: this.state.info[0].fname,
          lname: this.state.info[0].lname,
          bday: this.state.info[0].dob,
          cpnyName: this.state.info[0].cpnyName,
          cpnySector: this.state.info[0].cpnySector,
          cpnySize: this.state.info[0].cpnySize,
          cpnyStartDate: this.state.info[0].cpnyStartDate,
          cpnyTitle: this.state.info[0].cpnyTitle,
          cpnySummary: this.state.info[0].cpnySummary,
          cpnyLoc: this.state.info[0].cpnyLocation,
          cpnyContact: this.state.info[0].cpnyContact,
          cpnyWebsite: this.state.info[0].cpnyWeb,
          cpnyDesc: this.state.info[0].cpnyDesc,
          cpnyLogoURL: this.state.info[0].cpnyLogo,
        }) 
      });
}

handleGetUserPoints(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get('https://'+serverphp+'/php/checkPoints.php?user='+user+'&pass='+pass)
      .then(res => {
         this.setState({points: res.data});
      });
}

handleLoadItems(id){
  const data = new FormData();
  data.append('adminID', id);
  axios.post('https://'+serverphp+'/php/loadItems.php', data)
    .then(res => {
     var items = JSON.parse(JSON.stringify(res.data));
     this.setState({items: items});
  });
};

handleLoadTransactionHistory(){
  axios.get('https://'+serverphp+'/php/loadTransactionHistory.php?user='+this.state.user+'&pass='+this.state.pass)
    .then(res => {
     var hist = JSON.parse(JSON.stringify(res.data));
     
     if(this.state.history.length != hist){
      this.setState({history: hist});
     }
  });
};

handleItemListChange(index, event) {
    var dscst = this.state.descstate.slice(); 
    dscst[index] = event.target.value; 
    this.setState({descstate: dscst}); 
}

toggle(index){
  var arrayvar = this.state.collapse.slice()
  arrayvar[index] = !arrayvar[index];
  this.setState({ collapse: arrayvar })
}

handleOrderItem(){
  this.setState({disableOrder: true});
  axios.get('https://'+serverphp+'/php/insertRedeemOrder.php?user='+this.state.user+'&pass='+this.state.pass+'&amount='+this.state.points+'&name='+this.state.iname+'&quantity='+this.state.valueQuan+'&price='+this.state.iprice+'&adminID='+this.state.cpnyAdminID)    
  .then(res => {
         this.handleCloseDialog();
         this.setState({valueQuan: 1, disableOrder: false, openResOrder: true, orderResult: res.data});
      });
}

handleRequestCloseOrder = () => {
  this.setState({
    openResOrder: false,
  });
};

srchChangeAdrs(event){
  this.setState({srchadrs: event.target.value});
}

handleKeyDownSrchAdrs(event){
  axios.get('https://'+serverphp+'/php/searchCoworking.php?x='+event.target.value)
      .then(res => {
        var cpny = JSON.parse(JSON.stringify(res.data));
        this.setState({admincpny: cpny}); 
        if(this.state.admincpny.length !== 0){
          document.getElementById("dropdown").style.display = "block";
        }else{
          document.getElementById("dropdown").style.display = "none";
        }
      });
}

hideDropdown(id, name){
  this.setState({cpnyAdminID: id, srchadrs: name});
  document.getElementById("dropdown").style.display = "none";
  this.handleLoadItems(id);
}

allowEditProfile() {
  this.setState({openalertedit: true, alertmsg: 'Editing enabled', showsave: true, editdisable: false });
}

handleRequestClose() {
  this.setState({openalertedit: false});
}

saveChanges() {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get('https://'+serverphp+'/php/updateCustomerProfile.php?fname='+this.state.fname+'&lname='+this.state.lname+
  '&bday='+this.state.bday+'&cpnyName='+this.state.cpnyName+'&cpnySector='+this.state.cpnySector+'&cpnySize='+this.state.cpnySize+
  '&cpnyStartDate='+this.state.cpnyStartDate+'&cpnyTitle='+this.state.cpnyTitle+'&cpnySummary='+this.state.cpnySummary+
  '&cpnyLoc='+this.state.cpnyLoc+'&cpnyContact='+this.state.cpnyContact+'&cpnyWebsite='+this.state.cpnyWebsite+
  '&cpnyDesc='+this.state.cpnyDesc+'&cpnyLogoURL='+this.state.cpnyLogoURL+'&user='+user+'&pass='+pass)
      .then(res => {
        this.setState({openalertedit: true, alertmsg: res.data, showsave: false,  editdisable: true});
      });
}

render(){
if(!this.state.session){
    return (<div className="loadParent"><div className="loadChild"><CircularProgress size={this.state.loadersize} thickness={this.state.loaderthick}/></div></div>);
}
const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleCloseDialog}
        disabled={this.state.disableOrder}
      />,
      <FlatButton
        label="OK"
        primary={true}
        onClick={this.handleOrderItem}
        disabled={this.state.disableOrder}
      />,
    ];

const CoCircleLogo = (<div className="divbig">
                        <Paper zDepth={1} circle={true} className="paperlogo">
                            <Avatar  src={require('./images/Logo.png')} size={40} style={{'border': '2px solid white'}}/>
                        </Paper>
                        <div>
                            <span className="titleLogo">COCIRCLE</span>
                        </div>
                        </div>);
const AdminLogo = (<div className="divbig" onClick={this.handlePopupMore}>
              <Avatar src={this.state.info[0].lnkPicUrl} size={45} style={{'margin-top': '9.5px'}} />
              <span className="txttruncatebig">{this.state.info[0].fname+' '+this.state.info[0].lname}</span>
              <span className="txttruncate">{this.state.info[0].email}</span>
              <Moreexpand className="expandicon" color="white"/>
              <Popover
                open={this.state.openpopup}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                onRequestClose={this.handleClosePopupMore}
                animation={PopoverAnimationVertical}
              >
                <Menu>
                  <MenuItem primaryText="Sign out" onClick={this.handleSignout}/>
                </Menu>
              </Popover>
             </div>);
 const MoreSmall = (
              <IconMenu
                style={{'margin-top':'2px'}}
                iconButtonElement={
                  <IconButton><MoreVertIcon color="white"/></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              >
                <div className="menuavatar">
                  <Avatar src={this.state.info[0].lnkPicUrl} size={45} style={{'margin-top': '5px'}}/>
                  <span className="txttruncatebig" style={{color: 'black', 'margin-top': '10px'}}>{this.state.info[0].fname+' '+this.state.info[0].lname}</span>
                  <span className="txttruncate" style={{color: 'black', 'margin-top': '25px'}}>{this.state.info[0].email}</span>
                </div>
                <MenuItem primaryText="Sign out" onClick={this.handleSignout}/>
              </IconMenu>
            );
  const {history} = this.state;
  const PointsUI = (<div>
                <div style={{padding: '7.5px'}}>
                <Paper zDepth={1} className="paperCon">
                <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>POINTS WALLET</Subheader>
                <Divider />
                <div style={{padding:'20px 10px 20px 10px'}} align="center">
                    <span style={{color: blue500, 'font-size': '25px'}}>{this.state.points} PTS</span>
                </div>
                <Divider/>
                </Paper>
                <Paper zDepth={1} className="paperCon">
                <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>TRANSACTIONS</Subheader>
                <Divider />
                {
                history.map((hi) => (
                      
                <div style={{'margin': '10px 15px 10px 15px'}}>
                <div className="row">
                    <div style={{width:'41px','margin-left': '15px'}}>
                        <div className="centered">
                        <p className="stretchtxt primarytxt">{hi.M}</p><p className="txtdate primarytxt">{hi.D}/{hi.Y}</p>
                        </div>
                    </div>
                    <div className="col" style={{'margin-left': '0px !important'}} align="left">
                        <Dotdotdot clamp={3} align="left" className="centered">
                        <p style={{'line-height': '1rem', 'margin':'0', 'padding': '0','font-size':'13px'}} className="primarytxt">{hi.desc}</p>                          
                        </Dotdotdot>
                    </div>
                    <div style={{width: '70px', 'margin-right':'15px'}} align="right">
                        <div className="centered">
                        {(hi.stat.indexOf('+') != -1)?
                        <p style={{color: '#4caf50', 'margin':'0', 'padding': '0', 'line-height':'1rem'}}>{hi.stat}</p>:
                        <p style={{color: '#F44336', 'margin':'0', 'padding': '0', 'line-height':'1rem'}}>{hi.stat}</p>
                        }
                        </div>
                    </div>
                </div>
                </div>
                ))}
                <Divider/>
                </Paper>
                </div>
                </div>);

  const RedeemUI = (
            <div style={{'margin': '10px 15px 10px 15px'}}>
            <div className="row" style={{width: '100% !important', 'margin-top': '20px'}}>
            <div className="col" style={{width: '100% !important'}}>
            <Paper zDepth={1}  style={{padding: '0px 10px 0px 10px', 'margin-left': '-10px', 'margin-right': '-10px'}} >
                        <div>
                        <div className="row">
                        <div className="col-sm-12">   
                          <div className="row">    
                          <div className="col">     
                          <TextField style={{'font-size': '20px'}} hintText="Search coworking space" underlineShow={false} value={this.state.srchadrs} fullWidth={true} hintStyle={styles.hintText} onChange={this.srchChangeAdrs} onKeyUp={this.handleKeyDownSrchAdrs}/>
                          <Paper align="left" id="dropdown" zDepth={1} style={{width: '107.5%', 'max-width': '107.5% !important','margin': '0 -10px 0 -10px'}} className="dropdownsrch">
                            <List>
                            {this.state.admincpny.map((company) => (
                                  <ListItem primaryText={company.name} onClick={() => this.hideDropdown(company.id, company.name)}/>
                                ))}
                            </List>
                          </Paper>
                          </div>
                          <div className="col-1">
                          <SearchIcon style={{width: 30, height: 30, color: '#9e9e9e', 'margin-top':'10px','position':'absolute', right: '10px'}}/>
                          </div>
                          </div>
                        </div>
                        </div>
                        </div>
                </Paper>
                </div>
              </div>
              <div className="row">
                {this.state.items.map((items) => (                    
                <div className="col-sm-6" style={{padding: '7.5px'}}>
                <Paper zDepth={1}>
                    <div className="prodImage">
                      <img src={'https://'+serverphp+'/php/Items/'+items.imagename} width="100%" height="100%"/>
                    </div>
                    <Divider/>
                    <div className="container">
                    <div style={{'margin': '10px 0px 10px 0px'}}>
                    <div className="row">
                        <div className="col-8" style={{'padding-right': '0'}} align="left">
                            <div className="centered">
                              <Dotdotdot clamp={1}>
                                <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0','font-size':'20px'}} className="primarytxt">{items.name}</p>                          
                              </Dotdotdot>
                              <Dotdotdot clamp={1}>
                                <p style={{color: blue500, 'line-height': '1.5rem', 'margin':'0', 'padding': '0','font-size':'20px', 'font-weight': 'bold'}} className="primarytxt">{items.price} pts</p>                          
                              </Dotdotdot>
                            </div>
                        </div>
                        <div className="col-4" style={{'margin': '0', padding: '0'}} align="right">
                            <IconButton onClick={()=>this.toggle(items.id)} tooltipPosition="bottom-right" style={styles.small} iconStyle={styles.smallIcon}>
                              <Moreexpand/>
                            </IconButton>
                            <IconButton onClick={()=>this.handleOpenDialog(items.name, items.price)} tooltipPosition="bottom-right" style={styles.small} iconStyle={styles.smallIcon}>
                              <Redeem/>
                            </IconButton>   
                        </div>
                    </div>
                    </div>
                    </div>
                    <Divider/>
                     <Collapse isOpen={this.state.collapse[items.id]}>
                      <div style={{'margin': '15px'}}>
                        <p className="primarytxt" style={{padding: '0', margin: '0', 'line-height': '1rem'}}>
                          {items.desc}
                        </p>
                      </div>
                    </Collapse>
                    <Divider/>
                </Paper> 
                </div>
                ))}
                            
                <Dialog
                  contentStyle={dialogStyle}
                  title="Redeem"
                  actions={actions}
                  modal={false}
                  open={this.state.opendlred}
                  onRequestClose={this.handleClose}
                >
                  <div style={{'font-weight': '700', 'background-color': '#fafafa', 'padding': '10px', 'border': '0.5px solid #e0e0e0','border-radius': '2px'}}>
                      <span style={{'font-weight': '700'}}>YOUR POINTS:</span><span style={{color: blue500, float: 'right'}}>{this.state.points} PTS</span>
                      <br/>
                      <span style={{'font-weight': '700'}}>ITEM PRICE:</span><span style={{color: '#F44336', float: 'right'}}>-{this.state.iprice} PTS</span>
                      <br/>
                      <span style={{'font-weight': '700'}}>TOTAL:</span><span style={{color: '#4CAF50', float: 'right'}}>{this.state.itotal} PTS</span>                  
                      <br/>
                      <div className="row"> 
                      <div className="col-5">
                      <span style={{'font-weight': '700'}}>ITEM NAME:</span>
                      </div>
                      <div className="col-7" align="right">
                        <p style={{'margin':'0', 'padding': '0', 'font-weight':'normal'}} className="primarytxt">
                          {this.state.iname}
                        </p>    
                      </div>
                      </div>
                  </div>
                 
                  <SelectField
                    fullWidth={true}
                    underlineStyle={styles.underlineStyle} 
                    floatingLabelStyle={styles.labelStyle} 
                    floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                    floatingLabelText="Quantity"
                    value={this.state.valueQuan}
                    onChange={this.handleChangeQuan}
                    disabled={this.state.disableOrder}
                  >
                    <MenuItem value={1} primaryText="1" />
                    <MenuItem value={2} primaryText="2" />
                    <MenuItem value={3} primaryText="3" />
                    <MenuItem value={4} primaryText="4" />
                    <MenuItem value={5} primaryText="5" />
                  </SelectField>
                </Dialog>
                </div>
                </div>);

  const ProfileUI = (<div>
                <div style={{padding: '7.5px'}}>
                <Paper zDepth={1} className="paperCon" align="center" style={{padding: '20px'}}>
                  <Avatar src={this.state.info[0].lnkPicUrl} size={125}/>
                  <br/>
                  <br/>
                  <h4 className="primarytxt">{this.state.info[0].fname+' '+this.state.info[0].lname}</h4>
                </Paper>
                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>PERSONAL INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Firstname:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({fname: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Firstname" underlineShow={false}  fullWidth={true} value={this.state.fname}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Lastname:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({lname: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Lastname" underlineShow={false} fullWidth={true} value={this.state.lname}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Birthday:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({bday: e.target.value})} type="date" disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Birthday" underlineShow={false}  fullWidth={true} value={this.state.bday}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Email:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Email" underlineShow={false}  fullWidth={true} value={this.state.info[0].email}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Mobile:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Mobile" underlineShow={false}  fullWidth={true} value={this.state.info[0].mobile}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>LINKEDIN INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Headline:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField disabled={true} inputStyle={{color: 'black !important'}} hintText="Headline" underlineShow={false}  fullWidth={true} value={this.state.info[0].lnkHeadline}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Profile URL:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField disabled={true} inputStyle={{color: 'black !important'}} hintText="Profile URL" underlineShow={false} fullWidth={true} value={this.state.info[0].lnkProfileUrl}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Location:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField disabled={true} inputStyle={{color: 'black !important'}} hintText="Location" underlineShow={false}  fullWidth={true} value={this.state.info[0].lnkLocation}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Summary:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField disabled={true} inputStyle={{color: 'black !important'}} hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.info[0].lnkSummary}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Specialties:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField disabled={true} inputStyle={{color: 'black !important'}} hintText="Specialties" underlineShow={false}  fullWidth={true} value={this.state.info[0].lnkSpecialties}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>COMPANY INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Name:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyName: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Name" underlineShow={false}  fullWidth={true} value={this.state.cpnyName}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Sector:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnySector: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Sector" underlineShow={false} fullWidth={true} value={this.state.cpnySector}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Size:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnySize: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Size" underlineShow={false}  fullWidth={true} value={this.state.cpnySize}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Start date:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyStartDate: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Start date" underlineShow={false}  fullWidth={true} value={this.state.cpnyStartDate}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Summary:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnySummary: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.cpnySummary}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Location:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyLoc: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Location" underlineShow={false}  fullWidth={true} value={this.state.cpnyLoc}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Contact:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyContact: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Contact" underlineShow={false}  fullWidth={true} value={this.state.cpnyContact}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Website:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyWebsite: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Website" underlineShow={false}  fullWidth={true} value={this.state.cpnyWeb}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Title:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyTitle: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.cpnyTitle}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Description:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyDesc: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Description" underlineShow={false}  fullWidth={true} value={this.state.cpnyDesc}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Logo URL:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField onChange={(e)=> this.setState({cpnyLogoURL: e.target.value})} disabled={this.state.editdisable} inputStyle={{color: 'black !important'}} hintText="Logo URL" underlineShow={false}  fullWidth={true} value={this.state.cpnyLogoURL}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                </div>
                </div>);


    return(
    <div>
        <HeadRoom>
        <AppBar
          zDepth={0}
          title={<div id="titleid" className="titleStyle"><span className="titleFont">{this.state.title}</span></div>}
          onLeftIconButtonClick	= {this.handleToggleDim}
          iconElementLeft={CoCircleLogo}
          iconElementRight={(this.state.more)?AdminLogo:MoreSmall}
        />
        <Paper zDepth={2}>     
                <div style={{'max-width': '800px', 'margin-left': 'auto', 'margin-right': 'auto'}}>
                    <Tabs onChange={this.handleChangeTab}
                    value={this.state.slideIndex} style={styles.tabs}>
                        <Tab
                        value={0}
                        icon={<Star style={{color: this.state.t1}}/>}
                        label={<span style={{color: this.state.t1}}>POINTS</span>}
                        />
                        <Tab
                        value={1}
                        icon={<Redeem style={{color: this.state.t2}}/>}
                        label={<span style={{color: this.state.t2}}>REDEEM</span>}
                        />
                        <Tab
                        value={2}
                        icon={<MapsPersonPin style={{color: this.state.t3}}/>}
                        label={<span style={{color: this.state.t3}}>PROFILE</span>}
                        />
                    </Tabs>
                </div>
        </Paper>
        </HeadRoom>
        <div className="contentContainerMain">
            <div className="conMain">
                 <SwipeableViews
                  style={{'height':this.state.svheight,'min-height':'100vh', 'overflow':'hidden'}}
                  index={this.state.slideIndex}
                  onChangeIndex={this.handleChangeTab}>
                  <div id="div1">
                    {PointsUI}
                  </div>
                  <div id="div2">
                    {RedeemUI}
                  </div>
                  <div id="div3">
                    {ProfileUI}
                  </div>
                </SwipeableViews>
            </div>
        </div>
        {
          this.state.updateprofbtn?
          <FloatingActionButton className="FloatingbtnCust" onClick={this.allowEditProfile}>
            <ContentEdit />
          </FloatingActionButton>
          :null
        }
        { 
          this.state.updateprofbtn && this.state.showsave?
          <FloatingActionButton className="FloatingbtnCust" onClick={this.saveChanges}>
            <ContentSave />
          </FloatingActionButton>
          :null
        }
        
        <Snackbar
          open={this.state.openalertedit}
          message={this.state.alertmsg}
          autoHideDuration={4000}
          onRequestClose={this.handleRequestClose}
        />

        <Snackbar
          open={this.state.openResOrder}
          message={this.state.orderResult}
          autoHideDuration={4000}
          onRequestClose={this.handleRequestCloseOrder}
        />
    </div>);
}
  
}

